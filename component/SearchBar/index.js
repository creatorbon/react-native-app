import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
} from "react-native";

const SearchBar = ({ onSearch }) => {
  const [search, setSearch] = useState("");

  useEffect(() => {
    onSearch(search);
  }, [search]);

  return (
    <TouchableWithoutFeedback>
      <View>
        <TextInput
          style={styles.formInput}
          value={search}
          onChangeText={(search) => setSearch(search)}
          placeholder="Search"
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  formInput: {
    alignSelf: "stretch",
    textAlign: "center",
    borderColor: "#909192",
    borderWidth: 2,
    height: 50,
    paddingVertical: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 5,
  },
});

export default SearchBar;

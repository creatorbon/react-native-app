import React from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image } from "react-native";

const ContactItem = ({ item, onPress, onDelete }) => (
  <TouchableOpacity
    onPress={onPress}
    onLongPress={() => onDelete(item.id)}
    style={styles.item}
  >
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        source={{
          uri: item.avatar,
        }}
      />
      <View>
        <View style={styles.profileName}>
          <Text style={styles.profileNameContent}>{item.firstName}</Text>
          <Text style={styles.profileNameContent}>{item.lastName}</Text>
        </View>
        <Text style={styles.title}>{item.phone}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    backgroundColor: "#dcdbdb",
  },
  profileName: {
    flexDirection: "row",
  },
  profileNameContent: {
    fontSize: 30,
    marginRight: 5,
  },
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
  avatar: {
    width: 50,
    height: 50,
  },
});

export default ContactItem;

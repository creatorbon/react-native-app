import React from "react";
import { FlatList, SafeAreaView, StyleSheet } from "react-native";
import ContactItem from "../ContactItem";

const ContactList = ({ navigate, onDelete, contacts }) => {
  const renderItem = ({ item }) => {
    return (
      <ContactItem
        item={item}
        onPress={() =>
          navigate("Profile", { onDelete: onDelete, profile: item })
        }
        onDelete={onDelete}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={contacts}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingBottom: 60,
  },
});

export default ContactList;

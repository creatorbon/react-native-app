export const data = [
  {
    id: "d33b859b-3712-4ab6-8c88-a6c7ad50604c",
    firstName: "Betsey",
    lastName: "MattiCCI",
    phone: "+55 (241) 875-3304",
    avatar:
      "https://robohash.org/perspiciatisutdebitis.jpg?size=200x200&set=set1",
  },
  {
    id: "7832bd34-fb6d-450b-b56c-f9f3be42a35a",
    firstName: "Ulises",
    lastName: "Manuello",
    phone: "+66 (619) 308-5951",
    avatar: "https://robohash.org/natusfugavero.png?size=200x200&set=set1",
  },
  {
    id: "a381689c-67de-48b1-bdaf-e01a5ce05b14",
    firstName: "Pauly",
    lastName: "Armfirld",
    phone: "+33 (245) 274-7975",
    avatar: "https://robohash.org/optioquiquia.jpg?size=200x200&set=set1",
  },
  {
    id: "37b21132-3019-48d5-bcb9-9343159cd509",
    firstName: "Jacob",
    lastName: "Ramelot",
    phone: "+976 (926) 738-0745",
    avatar: "https://robohash.org/etmaioreshic.jpg?size=200x200&set=set1",
  },
  {
    id: "e72de140-4625-49bc-b6b1-c28d5e95e68a",
    firstName: "Benni",
    lastName: "Yakushkin",
    phone: "+351 (602) 121-7396",
    avatar: "https://robohash.org/nihildoloresquia.jpg?size=200x200&set=set1",
  },
  {
    id: "39c47062-5a76-44f9-b4a3-02b035599be5",
    firstName: "Fenelia",
    lastName: "Beldan",
    phone: "+62 (686) 296-4702",
    avatar:
      "https://robohash.org/dignissimossimiliquenon.bmp?size=200x200&set=set1",
  },
  {
    id: "515ccc86-c19e-46cd-b001-b471596ef42c",
    firstName: "Gabie",
    lastName: "Filippyev",
    phone: "+7 (445) 930-2112",
    avatar: "https://robohash.org/estutsunt.jpg?size=200x200&set=set1",
  },
  {
    id: "e0861c3d-8544-4c74-98ce-082558951764",
    firstName: "Harvey",
    lastName: "Verbruggen",
    phone: "+250 (274) 749-2739",
    avatar: "https://robohash.org/sitcommodivelit.png?size=200x200&set=set1",
  },
  {
    id: "c04e25d4-3e34-4331-affe-e5411709b8cc",
    firstName: "Jen",
    lastName: "Cianni",
    phone: "+1 (904) 202-9410",
    avatar: "https://robohash.org/voluptatemculpaquo.bmp?size=200x200&set=set1",
  },
  {
    id: "9a5d70be-cbb6-4163-9300-f6ffe9be8c9d",
    firstName: "Terrill",
    lastName: "Hardwick",
    phone: "+86 (551) 459-0236",
    avatar: "https://robohash.org/nihiletperferendis.png?size=200x200&set=set1",
  },
];

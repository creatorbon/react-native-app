import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { YellowBox } from "react-native";
import Profile from "./screens/Profile";
import AddProfile from "./screens/AddProfile";
import Contacts from "./screens/Contacts";

YellowBox.ignoreWarnings([
  "Non-serializable values were found in the navigation state",
]);

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Contacts}
          options={{ title: "Welcome to Contacts" }}
        />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="AddProfile" component={AddProfile} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

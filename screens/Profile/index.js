import React from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Linking,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

const Profile = ({ route, navigation: { goBack } }) => {
  const { onDelete, profile } = route.params;
  const { avatar, phone, lastName, firstName, id } = profile;
  return (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        source={{
          uri: avatar,
        }}
      />
      <View>
        <View style={styles.profileName}>
          <Text style={styles.profileNameContent}>{firstName}</Text>
          <Text style={styles.profileNameContent}>{lastName}</Text>
        </View>
        <Text style={styles.profilePhoneNumber}>{phone}</Text>
      </View>
      <View style={styles.profileButtons}>
        <TouchableOpacity onPress={() => Linking.openURL(`tel:${phone}`)}>
          <View style={styles.profileButton}>
            <Icon style={styles.profileIcon} name="phone" />
            <Text style={styles.profileButtonTitle}> Call </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            onDelete(id);
            goBack();
          }}
        >
          <View style={styles.profileButton}>
            <Text style={styles.profileButtonTitle}> Delete </Text>
            <Icon style={styles.profileIcon} name="times" />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  avatar: {
    width: 150,
    height: 150,
    marginBottom: 10,
  },
  profileName: {
    flexDirection: "row",
    justifyContent: "center",
  },
  profileNameContent: {
    fontSize: 30,
    margin: 10,
  },
  profilePhoneNumber: {
    fontSize: 30,
    margin: 10,
  },
  profileIcon: {
    marginRight: 10,
    color: "gray",
    fontSize: 20,
  },
  profileButtons: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 75,
  },
  profileButton: {
    width: 120,
    margin: 2,
    backgroundColor: "#dfdfdf",
    flexDirection: "row",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  profileButtonTitle: {
    fontSize: 20,
  },
});

export default Profile;

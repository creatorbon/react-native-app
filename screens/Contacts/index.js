import React, { useState } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import ContactList from "../../component/ContactList";
import { data } from "../../mockData";
import randomId from "../../helpers/randomId";
import Icon from "react-native-vector-icons/FontAwesome";
import SearchBar from "../../component/SearchBar";

const Contacts = ({ navigation }) => {
  const [contacts, setContacts] = useState(data);
  const [searchParams, setSearchParams] = useState("");

  const addContact = (firstName, lastName, phone) => {
    const id = randomId();
    const newContact = {
      id: id,
      firstName: firstName,
      lastName: lastName,
      phone: phone,
      avatar: "https://robohash.org/natusfugavero.png?size=200x200&set=set1",
    };
    setContacts([...contacts, newContact]);
  };

  const onDelete = (id) => {
    const updatedContacts = contacts.filter((contact) => contact.id !== id);
    setContacts(updatedContacts);
  };

  const onSearch = (value) => {
    setSearchParams(value);
  };

  const filterContacts = (params) => {
    const updatedContacts = contacts.filter((contact) => {
      const itemData = contact.firstName
        ? contact.firstName.toUpperCase()
        : "".toUpperCase();
      const textData = params.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    return updatedContacts;
  };

  return (
    <View style={styles.container}>
      <SearchBar onSearch={onSearch} />
      <ContactList
        navigate={navigation.navigate}
        onDelete={onDelete}
        contacts={filterContacts(searchParams)}
      ></ContactList>
      <TouchableOpacity
        style={styles.addButton}
        onPress={() =>
          navigation.navigate("AddProfile", { addContact: addContact })
        }
      >
        <Icon style={styles.addButtonIcon} name="plus" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  addButton: {
    position: "absolute",
    bottom: 10,
    right: 10,
    width: 75,
    height: 75,
    backgroundColor: "#0077cc7a",
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  addButtonIcon: {
    color: "#ffffff",
    fontSize: 20,
  },
});

export default Contacts;

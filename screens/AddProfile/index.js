import React, { useState } from "react";
import { Text, View, TouchableOpacity, StyleSheet, Alert } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { TextInput } from "react-native-gesture-handler";

const AddProfile = ({ route, navigation: { goBack } }) => {
  const { addContact } = route.params;
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phone, setPhone] = useState("");

  const onAdd = () => {
    if (!firstName.length || !lastName.length || !phone.length) {
      Alert.alert("All field are require ");
      return;
    }
    if (!phone.match(/^\d+$/)) {
      Alert.alert("Invalid phone number");
      return;
    }
    addContact(firstName, lastName, phone);
    goBack();
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.formInput}
        value={firstName}
        onChangeText={(firstName) => setFirstName(firstName)}
        placeholder="Enter First Name"
      />
      <TextInput
        style={styles.formInput}
        value={lastName}
        onChangeText={(lastName) => setLastName(lastName)}
        placeholder="Enter Last Name"
      />
      <TextInput
        style={styles.formInput}
        value={phone}
        onChangeText={(phone) => setPhone(phone)}
        placeholder="Enter Phone Number"
      />

      <TouchableOpacity onPress={onAdd}>
        <View style={styles.addButton}>
          <Text style={styles.addButtonTitle}> Add </Text>
          <Icon style={styles.addButtonIcon} name="address-book" />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  formInput: {
    alignSelf: "stretch",
    textAlign: "center",
    borderColor: "#909192",
    borderWidth: 2,
    height: 50,
    paddingVertical: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 5,
  },
  addButton: {
    flexDirection: "row",
    marginTop: 20,
    width: 200,
    height: 50,
    backgroundColor: "#dfdfdf",
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  addButtonTitle: {
    fontSize: 25,
  },
  addButtonIcon: {
    fontSize: 20,
    opacity: 0.7,
  },
});

export default AddProfile;
